package poop;

import java.util.Scanner;
import java.util.Random;
import static java.lang.System.out;

public class Assignment5 {
    public static final String dromsack = "dromsack";
    public static final String tjena = "tjena";
    public static final String insvep = "insvep";
    public static final String nordli = "nordli";
    public static final String lillasjon = "lillasjon";

    public static void main(String[] args)
    {
        rules();
        String yesorno = play();
        int rounds = 0;
        int playerscore = 0;
        int compscore = 0;

        while (yesorno.equals("Y"))
        {
            String playermove = getPlayerMove();
            String compmove = getCompMove();
            rounds++;

            if (playermove.equals(compmove)) {
                out.println("Game is Tie!");
            }
            else if (playermove.equals("lillasjon") && compmove.equals("insvep")) {
                System.out.println("Player wins!");
                playerscore++;

            }
            else if (playermove.equals("lillasjon") && compmove.equals("dromsack")) {
                System.out.println("Player wins!");
                playerscore++;
            }
            else if (playermove.equals("insvep") && compmove.equals("lillasjon")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("insvep") && compmove.equals("lillasjon")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("insvep") && compmove.equals("tjena")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("insvep") && compmove.equals("norli")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("nordli") && compmove.equals("insvep")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("tjena") && compmove.equals("insvep")) {
                System.out.println("Computer Wins!");
                compscore++;

            }
            else if (playermove.equals("tjena") && compmove.equals("lillasjon")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("tjena") && compmove.equals("dromsack")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("lillasjon") && compmove.equals("tjena")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("dromsack") && compmove.equals("tjena")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("dromsack") && compmove.equals("nordli")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("dromsack") && compmove.equals("insvep")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("nordli") && compmove.equals("dromsack")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("insvep") && compmove.equals("dromsack")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else if (playermove.equals("nordli") && compmove.equals("lillasjon")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("nordli") && compmove.equals("tjena")) {
                System.out.println("Player Wins!");
                playerscore++;
            }
            else if (playermove.equals("lillasjon") && compmove.equals("nordli")) {
                System.out.println("Computer Wins!");
                compscore++;
            }
            else
                System.out.println("Computer Wins!");

            yesorno = play();
        }
        if (rounds > 1){
            rounds-= 1;
        }
        out.println("Rounds Played: " + rounds );
        out.println("Player Score: " + playerscore);
        out.println("Computer Score: " + compscore);



    }
    public static void rules() {
        out.println("During each round, players choose a move, which may be either Dromsack, Tjena, Insvep, Nordli, or Lillasjon. The rules are:\n" +
                "\n" +
                "Lillasjon beats Insvep, Dromsack\n" +
                "Insvep beats Tjena, Nordli\n" +
                "Tjena beats Lillasjon, Dromsack\n" +
                "Dromsack Beats Nordli, Insvep\n" +
                "Nordli beats Lillasjon, Tjena");

    }
    
    public static String play()
    {
        out.println("Enter Y to play, N to not play: ");
        Scanner scanner = new Scanner(System.in);
        String userinput = scanner.nextLine();

        return userinput;
    }
    
    public static String getPlayerMove()
    {
    	out.println("Please enter a move: dromsack, tjena, insvep, nordli, lillasjon");
        String playermove = "";
        Scanner scanner = new Scanner(System.in);
       
        while (true) {
        	playermove = scanner.nextLine();

            if (playermove.equals("lillasjon") || playermove.equals("insvep") || playermove.equals("tjena") || playermove.equals("dromsack") || playermove.equals("nordli")) {
                break;
            }else{
                out.println("Please enter a move: dromsack, tjena, insvep, nordli, lillasjon");
  
                continue;
            }
        }
        out.println("Player Move: " + playermove);
        return playermove;

    }
    public static String getCompMove()
    {

        String compmove;
        Random rnd = new Random();
        int num = rnd.nextInt(5);
        if (num == 0) {
            compmove = "dromsack";
        }
        else if (num == 1){
            compmove = "tjena";
        }
        else if (num == 2){
            compmove = "insvep";
        }
        else if (num == 3){
            compmove = "nordli";
        }
        else{
            compmove = "lillasjon";

        }
        out.println("Computer Move: " + compmove);
        return compmove;


    }
}
